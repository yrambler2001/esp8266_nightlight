dofile("colours.lua")
dofile("ledfunctions.lua")
ws2812.init()

function pseudoSin(idx)
    idx = idx % 128
    lookUp = 32 - idx % 64
    val = 256 - (lookUp * lookUp) / 4

    if (idx > 64) then
        val = -val
    end

    return 256 + val
end

function hextoarr(sHex)
    return {tonumber(string.sub(sHex, 1, 2), 16), tonumber(string.sub(sHex, 3, 4), 16), tonumber(string.sub(sHex, 5, 6), 16)}
end

sColo = rFile("ledcolor.arg", "ffffff")
aiColo = hextoarr(sColo)
aFunc = rFile("ledfunc.arg", "1")
aFunc = "2"

sBr=255

i = 0
ii = 128 / 2
dir = 0
a = 0
aa = 0
aaa = 0
b = 0
bb = 0
bbb = 0
timz = 0
maxx = 254
q = 1
doneCode = "-"
buffer = ws2812.newBuffer(174, 3)
ws2812_effects.init(buffer)
buffer:fill(0, 0, 0)

tmr.create():alarm(25, 1, function()
    if aFunc == "1" then --static
        if doneCode ~= "1" then
            doneCode = aFunc
            ws2812_effects.stop()
            ws2812_effects.set_color(aiColo[2], aiColo[1], aiColo[3])
            ws2812_effects.set_brightness(sBr)
            ws2812_effects.set_mode("static")
            ws2812_effects.start()
end
    elseif aFunc == "2" then --gradient
        if doneCode ~= "2" then
            doneCode = aFunc
            ws2812_effects.stop()
            ws2812_effects.set_speed(250)
            ws2812_effects.set_brightness(255)
            ws2812_effects.set_mode("gradient", string.char(0, 200, 0, 200, 200, 0, 0, 200, 0))
            ws2812_effects.start()
        end
        
    elseif aFunc == "3" then --rainbow
        if doneCode ~= "3" then
            doneCode = aFunc
            ws2812_effects.stop()
            ws2812_effects.set_speed(250)
            ws2812_effects.set_brightness(255)
            ws2812_effects.set_mode("rainbow_cycle")
            ws2812_effects.start()
        end
        
    elseif aFunc == "4" then --random
        if ((doneCode == "2") or (doneCode == "3")) then
            ws2812_effects.stop()
        end
        doneCode = aFunc
        i = tst(i + 1, 1)
        ii = tst(ii + 1, 2)
        buffer:fill(ts1(a * ts(i) + b * ts(ii)), ts1(aa * ts(i) + bb * ts(ii)), ts1(aaa * ts(i) + bbb * ts(ii)))
        ws2812.write(buffer)
    end
end)
